# AwsSdkCognitoIdp

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `aws_sdk_cognito_idp` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:aws_sdk_cognito_idp, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/aws_sdk_cognito_idp](https://hexdocs.pm/aws_sdk_cognito_idp).

