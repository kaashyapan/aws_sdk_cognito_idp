defmodule AwsSdk.CognitoIdp.MixProject do
  use Mix.Project

  def project do
    [
      app: :aws_sdk_cognito_idp,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:aws_sdk, git: "git@gitlab.com:kaashyapan/aws_sdk_core.git"},
      {:dialyxir, "~> 1.0.0-rc.7", only: :dev},
      {:credo, "~> 1.2.0-rc1", only: :dev}
    ]
  end
end
