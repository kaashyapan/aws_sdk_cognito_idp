defmodule AwsSdk.CognitoIdp do
  @moduledoc """
  Using the Amazon Cognito User Pools API, you can create a user pool to manage directories and users. You can authenticate a user to obtain tokens related to user identity and access policies.
  This API reference provides information about user pools in Amazon Cognito User Pools.
  For more information, see the Amazon Cognito Documentation.
  """

  @type cognito_request() ::
          add_custom_attributes_request
          | admin_add_user_to_group_request
          | admin_confirm_sign_up_request
          | admin_create_user_request
          | admin_delete_user_request
          | admin_delete_user_attributes_request
          | admin_disable_provider_for_user_request
          | admin_disable_user_request
          | admin_enable_user_request
          | admin_forget_device_request
          | admin_get_device_request
          | admin_get_user_request
          | admin_initiate_auth_request
          | admin_link_provider_for_user_request
          | admin_list_devices_request
          | admin_list_groups_for_user_request
          | admin_list_user_auth_events_request
          | admin_remove_user_from_group_request
          | admin_reset_user_password_request
          | admin_respond_to_auth_challenge_request
          | admin_set_user_m_f_a_preference_request
          | admin_set_user_password_request
          | admin_set_user_settings_request
          | admin_update_auth_event_feedback_request
          | admin_update_device_status_request
          | admin_update_user_attributes_request
          | admin_user_global_sign_out_request
          | associate_software_token_request
          | change_password_request
          | confirm_device_request
          | confirm_forgot_password_request
          | confirm_sign_up_request
          | create_group_request
          | create_identity_provider_request
          | create_resource_server_request
          | create_user_import_job_request
          | create_user_pool_request
          | create_user_pool_client_request
          | create_user_pool_domain_request
          | delete_group_request
          | delete_identity_provider_request
          | delete_resource_server_request
          | delete_user_request
          | delete_user_attributes_request
          | delete_user_pool_request
          | delete_user_pool_client_request
          | delete_user_pool_domain_request
          | describe_identity_provider_request
          | describe_resource_server_request
          | describe_risk_configuration_request
          | describe_user_import_job_request
          | describe_user_pool_request
          | describe_user_pool_client_request
          | describe_user_pool_domain_request
          | forget_device_request
          | forgot_password_request
          | get_c_s_v_header_request
          | get_device_request
          | get_group_request
          | get_identity_provider_by_identifier_request
          | get_signing_certificate_request
          | get_u_i_customization_request
          | get_user_request
          | get_user_attribute_verification_code_request
          | get_user_pool_mfa_config_request
          | global_sign_out_request
          | initiate_auth_request
          | list_devices_request
          | list_groups_request
          | list_identity_providers_request
          | list_resource_servers_request
          | list_tags_for_resource_request
          | list_user_import_jobs_request
          | list_user_pool_clients_request
          | list_user_pools_request
          | list_users_request
          | list_users_in_group_request
          | resend_confirmation_code_request
          | respond_to_auth_challenge_request
          | set_risk_configuration_request
          | set_u_i_customization_request
          | set_user_m_f_a_preference_request
          | set_user_pool_mfa_config_request
          | set_user_settings_request
          | sign_up_request
          | start_user_import_job_request
          | stop_user_import_job_request
          | tag_resource_request
          | untag_resource_request
          | update_auth_event_feedback_request
          | update_device_status_request
          | update_group_request
          | update_identity_provider_request
          | update_resource_server_request
          | update_user_attributes_request
          | update_user_pool_request
          | update_user_pool_client_request
          | update_user_pool_domain_request
          | verify_software_token_request
          | verify_user_attribute_request

  @type cognito_response() ::
          add_custom_attributes_response
          | admin_add_user_to_group_response
          | admin_confirm_sign_up_response
          | admin_create_user_response
          | admin_delete_user_response
          | admin_delete_user_attributes_response
          | admin_disable_provider_for_user_response
          | admin_disable_user_response
          | admin_enable_user_response
          | admin_forget_device_response
          | admin_get_device_response
          | admin_get_user_response
          | admin_initiate_auth_response
          | admin_link_provider_for_user_response
          | admin_list_devices_response
          | admin_list_groups_for_user_response
          | admin_list_user_auth_events_response
          | admin_remove_user_from_group_response
          | admin_reset_user_password_response
          | admin_respond_to_auth_challenge_response
          | admin_set_user_m_f_a_preference_response
          | admin_set_user_password_response
          | admin_set_user_settings_response
          | admin_update_auth_event_feedback_response
          | admin_update_device_status_response
          | admin_update_user_attributes_response
          | admin_user_global_sign_out_response
          | associate_software_token_response
          | change_password_response
          | confirm_device_response
          | confirm_forgot_password_response
          | confirm_sign_up_response
          | create_group_response
          | create_identity_provider_response
          | create_resource_server_response
          | create_user_import_job_response
          | create_user_pool_response
          | create_user_pool_client_response
          | create_user_pool_domain_response
          | delete_group_response
          | delete_identity_provider_response
          | delete_resource_server_response
          | delete_user_response
          | delete_user_attributes_response
          | delete_user_pool_response
          | delete_user_pool_client_response
          | delete_user_pool_domain_response
          | describe_identity_provider_response
          | describe_resource_server_response
          | describe_risk_configuration_response
          | describe_user_import_job_response
          | describe_user_pool_response
          | describe_user_pool_client_response
          | describe_user_pool_domain_response
          | forget_device_response
          | forgot_password_response
          | get_c_s_v_header_response
          | get_device_response
          | get_group_response
          | get_identity_provider_by_identifier_response
          | get_signing_certificate_response
          | get_u_i_customization_response
          | get_user_response
          | get_user_attribute_verification_code_response
          | get_user_pool_mfa_config_response
          | global_sign_out_response
          | initiate_auth_response
          | list_devices_response
          | list_groups_response
          | list_identity_providers_response
          | list_resource_servers_response
          | list_tags_for_resource_response
          | list_user_import_jobs_response
          | list_user_pool_clients_response
          | list_user_pools_response
          | list_users_response
          | list_users_in_group_response
          | resend_confirmation_code_response
          | respond_to_auth_challenge_response
          | set_risk_configuration_response
          | set_u_i_customization_response
          | set_user_m_f_a_preference_response
          | set_user_pool_mfa_config_response
          | set_user_settings_response
          | sign_up_response
          | start_user_import_job_response
          | stop_user_import_job_response
          | tag_resource_response
          | untag_resource_response
          | update_auth_event_feedback_response
          | update_device_status_response
          | update_group_response
          | update_identity_provider_response
          | update_resource_server_response
          | update_user_attributes_response
          | update_user_pool_response
          | update_user_pool_client_response
          | update_user_pool_domain_response
          | verify_software_token_response
          | verify_user_attribute_response

  @type cognito_action() :: String.t()
  @spec make_request(AwsSdk.Core.client(), cognito_action(), cognito_request()) ::
          {:ok, cognito_response()} | {:error, AwsSdk.Core.aws_error()}
  defp make_request(client, request_type, _request_attrs) do
    AwsSdk.Core.make_request(client, request_type, %{name: "sunder"})
  end

  @doc """
  Adds additional user attributes to the user pool schema.
  """

  @type add_custom_attributes_request :: %{
          custom_attributes: [
            %{
              attribute_data_type: String.t(),
              developer_only_attribute: String.t(),
              mutable: String.t(),
              name: String.t(),
              number_attribute_constraints: %{max_value: String.t(), min_value: String.t()},
              required: String.t(),
              string_attribute_constraints: %{max_length: String.t(), min_length: String.t()}
            }
          ],
          user_pool_id: String.t()
        }

  @type add_custom_attributes_response :: nil

  @spec add_custom_attributes(AwsSdk.Core.client(), add_custom_attributes_request) ::
          {:ok, add_custom_attributes_response} | {:error, AwsSdk.Core.aws_error()}

  def add_custom_attributes(client, add_custom_attributes_request) do
    make_request(client, "AddCustomAttributes", add_custom_attributes_request)
  end

  @doc """
  Adds the specified user to the specified group.
  Calling this action requires developer credentials.
  """

  @type admin_add_user_to_group_request :: %{
          group_name: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_add_user_to_group_response :: nil

  @spec admin_add_user_to_group(AwsSdk.Core.client(), admin_add_user_to_group_request) ::
          {:ok, admin_add_user_to_group_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_add_user_to_group(client, admin_add_user_to_group_request) do
    make_request(client, "AdminAddUserToGroup", admin_add_user_to_group_request)
  end

  @doc """
  Confirms user registration as an admin without using a confirmation code. Works on any user.
  Calling this action requires developer credentials.
  """

  @type admin_confirm_sign_up_request :: %{
          client_metadata: %{String.t() => String.t()},
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_confirm_sign_up_response :: nil

  @spec admin_confirm_sign_up(AwsSdk.Core.client(), admin_confirm_sign_up_request) ::
          {:ok, admin_confirm_sign_up_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_confirm_sign_up(client, admin_confirm_sign_up_request) do
    make_request(client, "AdminConfirmSignUp", admin_confirm_sign_up_request)
  end

  @doc """
  Creates a new user in the specified user pool.
  If
  is not set, the default is to send a welcome message via email or phone (SMS).
  Alternatively, you can call AdminCreateUser with “SUPPRESS” for the
  parameter, and Amazon Cognito will not send any email.
  In either case, the user will be in the
  state until they sign in and change their password.
  AdminCreateUser requires developer credentials.
  """

  @type admin_create_user_request :: %{
          client_metadata: %{String.t() => String.t()},
          desired_delivery_mediums: [String.t()],
          force_alias_creation: String.t(),
          message_action: String.t(),
          temporary_password: String.t(),
          user_attributes: [%{name: String.t(), value: String.t()}],
          user_pool_id: String.t(),
          username: String.t(),
          validation_data: [%{name: String.t(), value: String.t()}]
        }

  @type admin_create_user_response :: %{
          user: %{
            attributes: [%{name: String.t(), value: String.t()}],
            enabled: String.t(),
            m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
            user_create_date: String.t(),
            user_last_modified_date: String.t(),
            user_status: String.t(),
            username: String.t()
          }
        }

  @spec admin_create_user(AwsSdk.Core.client(), admin_create_user_request) ::
          {:ok, admin_create_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_create_user(client, admin_create_user_request) do
    make_request(client, "AdminCreateUser", admin_create_user_request)
  end

  @doc """
  Deletes a user as an administrator. Works on any user.
  Calling this action requires developer credentials.
  """

  @type admin_delete_user_request :: %{user_pool_id: String.t(), username: String.t()}

  @type admin_delete_user_response :: nil

  @spec admin_delete_user(AwsSdk.Core.client(), admin_delete_user_request) ::
          {:ok, admin_delete_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_delete_user(client, admin_delete_user_request) do
    make_request(client, "AdminDeleteUser", admin_delete_user_request)
  end

  @doc """
  Deletes the user attributes in a user pool as an administrator. Works on any user.
  Calling this action requires developer credentials.
  """

  @type admin_delete_user_attributes_request :: %{
          user_attribute_names: [String.t()],
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_delete_user_attributes_response :: nil

  @spec admin_delete_user_attributes(AwsSdk.Core.client(), admin_delete_user_attributes_request) ::
          {:ok, admin_delete_user_attributes_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_delete_user_attributes(client, admin_delete_user_attributes_request) do
    make_request(client, "AdminDeleteUserAttributes", admin_delete_user_attributes_request)
  end

  @doc """
  Disables the user from signing in with the specified external (SAML or social) identity provider. If the user to disable is a Cognito User Pools native username + password user, they are not permitted to use their password to sign-in. If the user to disable is a linked external IdP user, any link between that user and an existing user is removed. The next time the external user (no longer attached to the previously linked
  ) signs in, they must create a new user account. See
  .
  This action is enabled only for admin access and requires developer credentials.
  The
  must match the value specified when creating an IdP for the pool.
  To disable a native username + password user, the
  value must be
  and the
  must be
  , with the
  being the name that is used in the user pool for the user.
  The
  must always be
  for social identity providers. The
  must always be the exact subject that was used when the user was originally linked as a source user.
  For de-linking a SAML identity, there are two scenarios. If the linked identity has not yet been used to sign-in, the
  and
  must be the same values that were used for the
  when the identities were originally linked in the
  call. (If the linking was done with
  set to
  , the same applies here). However, if the user has already signed in, the
  must be
  and
  must be the subject of the SAML assertion.
  """

  @type admin_disable_provider_for_user_request :: %{
          user: %{
            provider_attribute_name: String.t(),
            provider_attribute_value: String.t(),
            provider_name: String.t()
          },
          user_pool_id: String.t()
        }

  @type admin_disable_provider_for_user_response :: nil

  @spec admin_disable_provider_for_user(
          AwsSdk.Core.client(),
          admin_disable_provider_for_user_request
        ) :: {:ok, admin_disable_provider_for_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_disable_provider_for_user(client, admin_disable_provider_for_user_request) do
    make_request(client, "AdminDisableProviderForUser", admin_disable_provider_for_user_request)
  end

  @doc """
  Disables the specified user.
  Calling this action requires developer credentials.
  """

  @type admin_disable_user_request :: %{user_pool_id: String.t(), username: String.t()}

  @type admin_disable_user_response :: nil

  @spec admin_disable_user(AwsSdk.Core.client(), admin_disable_user_request) ::
          {:ok, admin_disable_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_disable_user(client, admin_disable_user_request) do
    make_request(client, "AdminDisableUser", admin_disable_user_request)
  end

  @doc """
  Enables the specified user as an administrator. Works on any user.
  Calling this action requires developer credentials.
  """

  @type admin_enable_user_request :: %{user_pool_id: String.t(), username: String.t()}

  @type admin_enable_user_response :: nil

  @spec admin_enable_user(AwsSdk.Core.client(), admin_enable_user_request) ::
          {:ok, admin_enable_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_enable_user(client, admin_enable_user_request) do
    make_request(client, "AdminEnableUser", admin_enable_user_request)
  end

  @doc """
  Forgets the device, as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_forget_device_request :: %{
          device_key: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_forget_device_response :: nil

  @spec admin_forget_device(AwsSdk.Core.client(), admin_forget_device_request) ::
          {:ok, admin_forget_device_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_forget_device(client, admin_forget_device_request) do
    make_request(client, "AdminForgetDevice", admin_forget_device_request)
  end

  @doc """
  Gets the device, as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_get_device_request :: %{
          device_key: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_get_device_response :: %{
          device: %{
            device_attributes: [%{name: String.t(), value: String.t()}],
            device_create_date: String.t(),
            device_key: String.t(),
            device_last_authenticated_date: String.t(),
            device_last_modified_date: String.t()
          }
        }

  @spec admin_get_device(AwsSdk.Core.client(), admin_get_device_request) ::
          {:ok, admin_get_device_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_get_device(client, admin_get_device_request) do
    make_request(client, "AdminGetDevice", admin_get_device_request)
  end

  @doc """
  Gets the specified user by user name in a user pool as an administrator. Works on any user.
  Calling this action requires developer credentials.
  """

  @type admin_get_user_request :: %{user_pool_id: String.t(), username: String.t()}

  @type admin_get_user_response :: %{
          enabled: String.t(),
          m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
          preferred_mfa_setting: String.t(),
          user_attributes: [%{name: String.t(), value: String.t()}],
          user_create_date: String.t(),
          user_last_modified_date: String.t(),
          user_m_f_a_setting_list: [String.t()],
          user_status: String.t(),
          username: String.t()
        }

  @spec admin_get_user(AwsSdk.Core.client(), admin_get_user_request) ::
          {:ok, admin_get_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_get_user(client, admin_get_user_request) do
    make_request(client, "AdminGetUser", admin_get_user_request)
  end

  @doc """
  Initiates the authentication flow, as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_initiate_auth_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          auth_flow: String.t(),
          auth_parameters: %{String.t() => String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          context_data: %{
            encoded_data: String.t(),
            http_headers: [%{header_name: String.t(), header_value: String.t()}],
            ip_address: String.t(),
            server_name: String.t(),
            server_path: String.t()
          },
          user_pool_id: String.t()
        }

  @type admin_initiate_auth_response :: %{
          authentication_result: %{
            access_token: String.t(),
            expires_in: String.t(),
            id_token: String.t(),
            new_device_metadata: %{device_group_key: String.t(), device_key: String.t()},
            refresh_token: String.t(),
            token_type: String.t()
          },
          challenge_name: String.t(),
          challenge_parameters: %{String.t() => String.t()},
          session: String.t()
        }

  @spec admin_initiate_auth(AwsSdk.Core.client(), admin_initiate_auth_request) ::
          {:ok, admin_initiate_auth_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_initiate_auth(client, admin_initiate_auth_request) do
    make_request(client, "AdminInitiateAuth", admin_initiate_auth_request)
  end

  @doc """
  Links an existing user account in a user pool (
  ) to an identity from an external identity provider (
  ) based on a specified attribute name and value from the external identity provider. This allows you to create a link from the existing user account to an external federated user identity that has not yet been used to sign in, so that the federated user identity can be used to sign in as the existing user account.
  For example, if there is an existing user with a username and password, this API links that user to a federated user identity, so that when the federated user identity is used, the user signs in as the existing user account.
  See also
  .
  This action is enabled only for admin access and requires developer credentials.
  """

  @type admin_link_provider_for_user_request :: %{
          destination_user: %{
            provider_attribute_name: String.t(),
            provider_attribute_value: String.t(),
            provider_name: String.t()
          },
          source_user: %{
            provider_attribute_name: String.t(),
            provider_attribute_value: String.t(),
            provider_name: String.t()
          },
          user_pool_id: String.t()
        }

  @type admin_link_provider_for_user_response :: nil

  @spec admin_link_provider_for_user(AwsSdk.Core.client(), admin_link_provider_for_user_request) ::
          {:ok, admin_link_provider_for_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_link_provider_for_user(client, admin_link_provider_for_user_request) do
    make_request(client, "AdminLinkProviderForUser", admin_link_provider_for_user_request)
  end

  @doc """
  Lists devices, as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_list_devices_request :: %{
          limit: String.t(),
          pagination_token: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_list_devices_response :: %{
          devices: [
            %{
              device_attributes: [%{name: String.t(), value: String.t()}],
              device_create_date: String.t(),
              device_key: String.t(),
              device_last_authenticated_date: String.t(),
              device_last_modified_date: String.t()
            }
          ],
          pagination_token: String.t()
        }

  @spec admin_list_devices(AwsSdk.Core.client(), admin_list_devices_request) ::
          {:ok, admin_list_devices_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_list_devices(client, admin_list_devices_request) do
    make_request(client, "AdminListDevices", admin_list_devices_request)
  end

  @doc """
  Lists the groups that the user belongs to.
  Calling this action requires developer credentials.
  """

  @type admin_list_groups_for_user_request :: %{
          limit: String.t(),
          next_token: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_list_groups_for_user_response :: %{
          groups: [
            %{
              creation_date: String.t(),
              description: String.t(),
              group_name: String.t(),
              last_modified_date: String.t(),
              precedence: String.t(),
              role_arn: String.t(),
              user_pool_id: String.t()
            }
          ],
          next_token: String.t()
        }

  @spec admin_list_groups_for_user(AwsSdk.Core.client(), admin_list_groups_for_user_request) ::
          {:ok, admin_list_groups_for_user_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_list_groups_for_user(client, admin_list_groups_for_user_request) do
    make_request(client, "AdminListGroupsForUser", admin_list_groups_for_user_request)
  end

  @doc """
  Lists a history of user activity and any risks detected as part of Amazon Cognito advanced security.
  """

  @type admin_list_user_auth_events_request :: %{
          max_results: String.t(),
          next_token: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_list_user_auth_events_response :: %{
          auth_events: [
            %{
              challenge_responses: [%{challenge_name: String.t(), challenge_response: String.t()}],
              creation_date: String.t(),
              event_context_data: %{
                city: String.t(),
                country: String.t(),
                device_name: String.t(),
                ip_address: String.t(),
                timezone: String.t()
              },
              event_feedback: %{
                feedback_date: String.t(),
                feedback_value: String.t(),
                provider: String.t()
              },
              event_id: String.t(),
              event_response: String.t(),
              event_risk: %{risk_decision: String.t(), risk_level: String.t()},
              event_type: String.t()
            }
          ],
          next_token: String.t()
        }

  @spec admin_list_user_auth_events(AwsSdk.Core.client(), admin_list_user_auth_events_request) ::
          {:ok, admin_list_user_auth_events_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_list_user_auth_events(client, admin_list_user_auth_events_request) do
    make_request(client, "AdminListUserAuthEvents", admin_list_user_auth_events_request)
  end

  @doc """
  Removes the specified user from the specified group.
  Calling this action requires developer credentials.
  """

  @type admin_remove_user_from_group_request :: %{
          group_name: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_remove_user_from_group_response :: nil

  @spec admin_remove_user_from_group(AwsSdk.Core.client(), admin_remove_user_from_group_request) ::
          {:ok, admin_remove_user_from_group_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_remove_user_from_group(client, admin_remove_user_from_group_request) do
    make_request(client, "AdminRemoveUserFromGroup", admin_remove_user_from_group_request)
  end

  @doc """
  Resets the specified user's password in a user pool as an administrator. Works on any user.
  When a developer calls this API, the current password is invalidated, so it must be changed. If a user tries to sign in after the API is called, the app will get a PasswordResetRequiredException exception back and should direct the user down the flow to reset the password, which is the same as the forgot password flow. In addition, if the user pool has phone verification selected and a verified phone number exists for the user, or if email verification is selected and a verified email exists for the user, calling this API will also result in sending a message to the end user with the code to change their password.
  Calling this action requires developer credentials.
  """

  @type admin_reset_user_password_request :: %{
          client_metadata: %{String.t() => String.t()},
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_reset_user_password_response :: nil

  @spec admin_reset_user_password(AwsSdk.Core.client(), admin_reset_user_password_request) ::
          {:ok, admin_reset_user_password_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_reset_user_password(client, admin_reset_user_password_request) do
    make_request(client, "AdminResetUserPassword", admin_reset_user_password_request)
  end

  @doc """
  Responds to an authentication challenge, as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_respond_to_auth_challenge_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          challenge_name: String.t(),
          challenge_responses: %{String.t() => String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          context_data: %{
            encoded_data: String.t(),
            http_headers: [%{header_name: String.t(), header_value: String.t()}],
            ip_address: String.t(),
            server_name: String.t(),
            server_path: String.t()
          },
          session: String.t(),
          user_pool_id: String.t()
        }

  @type admin_respond_to_auth_challenge_response :: %{
          authentication_result: %{
            access_token: String.t(),
            expires_in: String.t(),
            id_token: String.t(),
            new_device_metadata: %{device_group_key: String.t(), device_key: String.t()},
            refresh_token: String.t(),
            token_type: String.t()
          },
          challenge_name: String.t(),
          challenge_parameters: %{String.t() => String.t()},
          session: String.t()
        }

  @spec admin_respond_to_auth_challenge(
          AwsSdk.Core.client(),
          admin_respond_to_auth_challenge_request
        ) :: {:ok, admin_respond_to_auth_challenge_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_respond_to_auth_challenge(client, admin_respond_to_auth_challenge_request) do
    make_request(client, "AdminRespondToAuthChallenge", admin_respond_to_auth_challenge_request)
  end

  @doc """
  Sets the user's multi-factor authentication (MFA) preference, including which MFA options are enabled and if any are preferred. Only one factor can be set as preferred. The preferred MFA factor will be used to authenticate a user if multiple factors are enabled. If multiple options are enabled and no preference is set, a challenge to choose an MFA option will be returned during sign in.
  """

  @type admin_set_user_m_f_a_preference_request :: %{
          s_m_s_mfa_settings: %{enabled: String.t(), preferred_mfa: String.t()},
          software_token_mfa_settings: %{enabled: String.t(), preferred_mfa: String.t()},
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_set_user_m_f_a_preference_response :: nil

  @spec admin_set_user_m_f_a_preference(
          AwsSdk.Core.client(),
          admin_set_user_m_f_a_preference_request
        ) :: {:ok, admin_set_user_m_f_a_preference_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_set_user_m_f_a_preference(client, admin_set_user_m_f_a_preference_request) do
    make_request(client, "AdminSetUserMFAPreference", admin_set_user_m_f_a_preference_request)
  end

  @doc """
  Sets the specified user's password in a user pool as an administrator. Works on any user.
  The password can be temporary or permanent. If it is temporary, the user status will be placed into the
  state. When the user next tries to sign in, the InitiateAuth/AdminInitiateAuth response will contain the
  challenge. If the user does not sign in before it expires, the user will not be able to sign in and their password will need to be reset by an administrator.
  Once the user has set a new password, or the password is permanent, the user status will be set to
  .
  """

  @type admin_set_user_password_request :: %{
          password: String.t(),
          permanent: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_set_user_password_response :: nil

  @spec admin_set_user_password(AwsSdk.Core.client(), admin_set_user_password_request) ::
          {:ok, admin_set_user_password_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_set_user_password(client, admin_set_user_password_request) do
    make_request(client, "AdminSetUserPassword", admin_set_user_password_request)
  end

  @doc """
  You can use it to configure only SMS MFA. You can't use it to configure TOTP software token MFA. To configure either type of MFA, use the
  action instead.
  """

  @type admin_set_user_settings_request :: %{
          m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_set_user_settings_response :: nil

  @spec admin_set_user_settings(AwsSdk.Core.client(), admin_set_user_settings_request) ::
          {:ok, admin_set_user_settings_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_set_user_settings(client, admin_set_user_settings_request) do
    make_request(client, "AdminSetUserSettings", admin_set_user_settings_request)
  end

  @doc """
  Provides feedback for an authentication event as to whether it was from a valid user. This feedback is used for improving the risk evaluation decision for the user pool as part of Amazon Cognito advanced security.
  """

  @type admin_update_auth_event_feedback_request :: %{
          event_id: String.t(),
          feedback_value: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_update_auth_event_feedback_response :: nil

  @spec admin_update_auth_event_feedback(
          AwsSdk.Core.client(),
          admin_update_auth_event_feedback_request
        ) :: {:ok, admin_update_auth_event_feedback_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_update_auth_event_feedback(client, admin_update_auth_event_feedback_request) do
    make_request(client, "AdminUpdateAuthEventFeedback", admin_update_auth_event_feedback_request)
  end

  @doc """
  Updates the device status as an administrator.
  Calling this action requires developer credentials.
  """

  @type admin_update_device_status_request :: %{
          device_key: String.t(),
          device_remembered_status: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_update_device_status_response :: nil

  @spec admin_update_device_status(AwsSdk.Core.client(), admin_update_device_status_request) ::
          {:ok, admin_update_device_status_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_update_device_status(client, admin_update_device_status_request) do
    make_request(client, "AdminUpdateDeviceStatus", admin_update_device_status_request)
  end

  @doc """
  Updates the specified user's attributes, including developer attributes, as an administrator. Works on any user.
  For custom attributes, you must prepend the
  prefix to the attribute name.
  In addition to updating user attributes, this API can also be used to mark phone and email as verified.
  Calling this action requires developer credentials.
  """

  @type admin_update_user_attributes_request :: %{
          client_metadata: %{String.t() => String.t()},
          user_attributes: [%{name: String.t(), value: String.t()}],
          user_pool_id: String.t(),
          username: String.t()
        }

  @type admin_update_user_attributes_response :: nil

  @spec admin_update_user_attributes(AwsSdk.Core.client(), admin_update_user_attributes_request) ::
          {:ok, admin_update_user_attributes_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_update_user_attributes(client, admin_update_user_attributes_request) do
    make_request(client, "AdminUpdateUserAttributes", admin_update_user_attributes_request)
  end

  @doc """
  Signs out users from all devices, as an administrator. It also invalidates all refresh tokens issued to a user. The user's current access and Id tokens remain valid until their expiry. Access and Id tokens expire one hour after they are issued.
  Calling this action requires developer credentials.
  """

  @type admin_user_global_sign_out_request :: %{user_pool_id: String.t(), username: String.t()}

  @type admin_user_global_sign_out_response :: nil

  @spec admin_user_global_sign_out(AwsSdk.Core.client(), admin_user_global_sign_out_request) ::
          {:ok, admin_user_global_sign_out_response} | {:error, AwsSdk.Core.aws_error()}

  def admin_user_global_sign_out(client, admin_user_global_sign_out_request) do
    make_request(client, "AdminUserGlobalSignOut", admin_user_global_sign_out_request)
  end

  @doc """
  Returns a unique generated shared secret key code for the user account. The request takes an access token or a session string, but not both.
  """

  @type associate_software_token_request :: %{access_token: String.t(), session: String.t()}

  @type associate_software_token_response :: %{secret_code: String.t(), session: String.t()}

  @spec associate_software_token(AwsSdk.Core.client(), associate_software_token_request) ::
          {:ok, associate_software_token_response} | {:error, AwsSdk.Core.aws_error()}

  def associate_software_token(client, associate_software_token_request) do
    make_request(client, "AssociateSoftwareToken", associate_software_token_request)
  end

  @doc """
  Changes the password for a specified user in a user pool.
  """

  @type change_password_request :: %{
          access_token: String.t(),
          previous_password: String.t(),
          proposed_password: String.t()
        }

  @type change_password_response :: nil

  @spec change_password(AwsSdk.Core.client(), change_password_request) ::
          {:ok, change_password_response} | {:error, AwsSdk.Core.aws_error()}

  def change_password(client, change_password_request) do
    make_request(client, "ChangePassword", change_password_request)
  end

  @doc """
  Confirms tracking of the device. This API call is the call that begins device tracking.
  """

  @type confirm_device_request :: %{
          access_token: String.t(),
          device_key: String.t(),
          device_name: String.t(),
          device_secret_verifier_config: %{password_verifier: String.t(), salt: String.t()}
        }

  @type confirm_device_response :: %{user_confirmation_necessary: String.t()}

  @spec confirm_device(AwsSdk.Core.client(), confirm_device_request) ::
          {:ok, confirm_device_response} | {:error, AwsSdk.Core.aws_error()}

  def confirm_device(client, confirm_device_request) do
    make_request(client, "ConfirmDevice", confirm_device_request)
  end

  @doc """
  Allows a user to enter a confirmation code to reset a forgotten password.
  """

  @type confirm_forgot_password_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          confirmation_code: String.t(),
          password: String.t(),
          secret_hash: String.t(),
          user_context_data: %{encoded_data: String.t()},
          username: String.t()
        }

  @type confirm_forgot_password_response :: nil

  @spec confirm_forgot_password(AwsSdk.Core.client(), confirm_forgot_password_request) ::
          {:ok, confirm_forgot_password_response} | {:error, AwsSdk.Core.aws_error()}

  def confirm_forgot_password(client, confirm_forgot_password_request) do
    make_request(client, "ConfirmForgotPassword", confirm_forgot_password_request)
  end

  @doc """
  Confirms registration of a user and handles the existing alias from a previous user.
  """

  @type confirm_sign_up_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          confirmation_code: String.t(),
          force_alias_creation: String.t(),
          secret_hash: String.t(),
          user_context_data: %{encoded_data: String.t()},
          username: String.t()
        }

  @type confirm_sign_up_response :: nil

  @spec confirm_sign_up(AwsSdk.Core.client(), confirm_sign_up_request) ::
          {:ok, confirm_sign_up_response} | {:error, AwsSdk.Core.aws_error()}

  def confirm_sign_up(client, confirm_sign_up_request) do
    make_request(client, "ConfirmSignUp", confirm_sign_up_request)
  end

  @doc """
  Creates a new group in the specified user pool.
  Calling this action requires developer credentials.
  """

  @type create_group_request :: %{
          description: String.t(),
          group_name: String.t(),
          precedence: String.t(),
          role_arn: String.t(),
          user_pool_id: String.t()
        }

  @type create_group_response :: %{
          group: %{
            creation_date: String.t(),
            description: String.t(),
            group_name: String.t(),
            last_modified_date: String.t(),
            precedence: String.t(),
            role_arn: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec create_group(AwsSdk.Core.client(), create_group_request) ::
          {:ok, create_group_response} | {:error, AwsSdk.Core.aws_error()}

  def create_group(client, create_group_request) do
    make_request(client, "CreateGroup", create_group_request)
  end

  @doc """
  Creates an identity provider for a user pool.
  """

  @type create_identity_provider_request :: %{
          attribute_mapping: %{String.t() => String.t()},
          idp_identifiers: [String.t()],
          provider_details: %{String.t() => String.t()},
          provider_name: String.t(),
          provider_type: String.t(),
          user_pool_id: String.t()
        }

  @type create_identity_provider_response :: %{
          identity_provider: %{
            attribute_mapping: %{String.t() => String.t()},
            creation_date: String.t(),
            idp_identifiers: [String.t()],
            last_modified_date: String.t(),
            provider_details: %{String.t() => String.t()},
            provider_name: String.t(),
            provider_type: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec create_identity_provider(AwsSdk.Core.client(), create_identity_provider_request) ::
          {:ok, create_identity_provider_response} | {:error, AwsSdk.Core.aws_error()}

  def create_identity_provider(client, create_identity_provider_request) do
    make_request(client, "CreateIdentityProvider", create_identity_provider_request)
  end

  @doc """
  Creates a new OAuth2.0 resource server and defines custom scopes in it.
  """

  @type create_resource_server_request :: %{
          identifier: String.t(),
          name: String.t(),
          scopes: [%{scope_description: String.t(), scope_name: String.t()}],
          user_pool_id: String.t()
        }

  @type create_resource_server_response :: %{
          resource_server: %{
            identifier: String.t(),
            name: String.t(),
            scopes: [%{scope_description: String.t(), scope_name: String.t()}],
            user_pool_id: String.t()
          }
        }

  @spec create_resource_server(AwsSdk.Core.client(), create_resource_server_request) ::
          {:ok, create_resource_server_response} | {:error, AwsSdk.Core.aws_error()}

  def create_resource_server(client, create_resource_server_request) do
    make_request(client, "CreateResourceServer", create_resource_server_request)
  end

  @doc """
  Creates the user import job.
  """

  @type create_user_import_job_request :: %{
          cloud_watch_logs_role_arn: String.t(),
          job_name: String.t(),
          user_pool_id: String.t()
        }

  @type create_user_import_job_response :: %{
          user_import_job: %{
            cloud_watch_logs_role_arn: String.t(),
            completion_date: String.t(),
            completion_message: String.t(),
            creation_date: String.t(),
            failed_users: String.t(),
            imported_users: String.t(),
            job_id: String.t(),
            job_name: String.t(),
            pre_signed_url: String.t(),
            skipped_users: String.t(),
            start_date: String.t(),
            status: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec create_user_import_job(AwsSdk.Core.client(), create_user_import_job_request) ::
          {:ok, create_user_import_job_response} | {:error, AwsSdk.Core.aws_error()}

  def create_user_import_job(client, create_user_import_job_request) do
    make_request(client, "CreateUserImportJob", create_user_import_job_request)
  end

  @doc """
  Creates a new Amazon Cognito user pool and sets the password policy for the pool.
  """

  @type create_user_pool_request :: %{
          account_recovery_setting: %{
            recovery_mechanisms: [%{name: String.t(), priority: String.t()}]
          },
          admin_create_user_config: %{
            allow_admin_create_user_only: String.t(),
            invite_message_template: %{
              email_message: String.t(),
              email_subject: String.t(),
              s_m_s_message: String.t()
            },
            unused_account_validity_days: String.t()
          },
          alias_attributes: [String.t()],
          auto_verified_attributes: [String.t()],
          device_configuration: %{
            challenge_required_on_new_device: String.t(),
            device_only_remembered_on_user_prompt: String.t()
          },
          email_configuration: %{
            configuration_set: String.t(),
            email_sending_account: String.t(),
            from: String.t(),
            reply_to_email_address: String.t(),
            source_arn: String.t()
          },
          email_verification_message: String.t(),
          email_verification_subject: String.t(),
          lambda_config: %{
            create_auth_challenge: String.t(),
            custom_message: String.t(),
            define_auth_challenge: String.t(),
            post_authentication: String.t(),
            post_confirmation: String.t(),
            pre_authentication: String.t(),
            pre_sign_up: String.t(),
            pre_token_generation: String.t(),
            user_migration: String.t(),
            verify_auth_challenge_response: String.t()
          },
          mfa_configuration: String.t(),
          policies: %{
            password_policy: %{
              minimum_length: String.t(),
              require_lowercase: String.t(),
              require_numbers: String.t(),
              require_symbols: String.t(),
              require_uppercase: String.t(),
              temporary_password_validity_days: String.t()
            }
          },
          pool_name: String.t(),
          schema: [
            %{
              attribute_data_type: String.t(),
              developer_only_attribute: String.t(),
              mutable: String.t(),
              name: String.t(),
              number_attribute_constraints: %{max_value: String.t(), min_value: String.t()},
              required: String.t(),
              string_attribute_constraints: %{max_length: String.t(), min_length: String.t()}
            }
          ],
          sms_authentication_message: String.t(),
          sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()},
          sms_verification_message: String.t(),
          user_pool_add_ons: %{advanced_security_mode: String.t()},
          user_pool_tags: %{String.t() => String.t()},
          username_attributes: [String.t()],
          verification_message_template: %{
            default_email_option: String.t(),
            email_message: String.t(),
            email_message_by_link: String.t(),
            email_subject: String.t(),
            email_subject_by_link: String.t(),
            sms_message: String.t()
          }
        }

  @type create_user_pool_response :: %{
          user_pool: %{
            account_recovery_setting: %{
              recovery_mechanisms: [%{name: String.t(), priority: String.t()}]
            },
            admin_create_user_config: %{
              allow_admin_create_user_only: String.t(),
              invite_message_template: %{
                email_message: String.t(),
                email_subject: String.t(),
                s_m_s_message: String.t()
              },
              unused_account_validity_days: String.t()
            },
            alias_attributes: [String.t()],
            arn: String.t(),
            auto_verified_attributes: [String.t()],
            creation_date: String.t(),
            custom_domain: String.t(),
            device_configuration: %{
              challenge_required_on_new_device: String.t(),
              device_only_remembered_on_user_prompt: String.t()
            },
            domain: String.t(),
            email_configuration: %{
              configuration_set: String.t(),
              email_sending_account: String.t(),
              from: String.t(),
              reply_to_email_address: String.t(),
              source_arn: String.t()
            },
            email_configuration_failure: String.t(),
            email_verification_message: String.t(),
            email_verification_subject: String.t(),
            estimated_number_of_users: String.t(),
            id: String.t(),
            lambda_config: %{
              create_auth_challenge: String.t(),
              custom_message: String.t(),
              define_auth_challenge: String.t(),
              post_authentication: String.t(),
              post_confirmation: String.t(),
              pre_authentication: String.t(),
              pre_sign_up: String.t(),
              pre_token_generation: String.t(),
              user_migration: String.t(),
              verify_auth_challenge_response: String.t()
            },
            last_modified_date: String.t(),
            mfa_configuration: String.t(),
            name: String.t(),
            policies: %{
              password_policy: %{
                minimum_length: String.t(),
                require_lowercase: String.t(),
                require_numbers: String.t(),
                require_symbols: String.t(),
                require_uppercase: String.t(),
                temporary_password_validity_days: String.t()
              }
            },
            schema_attributes: [
              %{
                attribute_data_type: String.t(),
                developer_only_attribute: String.t(),
                mutable: String.t(),
                name: String.t(),
                number_attribute_constraints: %{max_value: String.t(), min_value: String.t()},
                required: String.t(),
                string_attribute_constraints: %{max_length: String.t(), min_length: String.t()}
              }
            ],
            sms_authentication_message: String.t(),
            sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()},
            sms_configuration_failure: String.t(),
            sms_verification_message: String.t(),
            status: String.t(),
            user_pool_add_ons: %{advanced_security_mode: String.t()},
            user_pool_tags: %{String.t() => String.t()},
            username_attributes: [String.t()],
            verification_message_template: %{
              default_email_option: String.t(),
              email_message: String.t(),
              email_message_by_link: String.t(),
              email_subject: String.t(),
              email_subject_by_link: String.t(),
              sms_message: String.t()
            }
          }
        }

  @spec create_user_pool(AwsSdk.Core.client(), create_user_pool_request) ::
          {:ok, create_user_pool_response} | {:error, AwsSdk.Core.aws_error()}

  def create_user_pool(client, create_user_pool_request) do
    make_request(client, "CreateUserPool", create_user_pool_request)
  end

  @doc """
  Creates the user pool client.
  """

  @type create_user_pool_client_request :: %{
          allowed_o_auth_flows: [String.t()],
          allowed_o_auth_flows_user_pool_client: String.t(),
          allowed_o_auth_scopes: [String.t()],
          analytics_configuration: %{
            application_id: String.t(),
            external_id: String.t(),
            role_arn: String.t(),
            user_data_shared: String.t()
          },
          callback_u_r_ls: [String.t()],
          client_name: String.t(),
          default_redirect_u_r_i: String.t(),
          explicit_auth_flows: [String.t()],
          generate_secret: String.t(),
          logout_u_r_ls: [String.t()],
          prevent_user_existence_errors: String.t(),
          read_attributes: [String.t()],
          refresh_token_validity: String.t(),
          supported_identity_providers: [String.t()],
          user_pool_id: String.t(),
          write_attributes: [String.t()]
        }

  @type create_user_pool_client_response :: %{
          user_pool_client: %{
            allowed_o_auth_flows: [String.t()],
            allowed_o_auth_flows_user_pool_client: String.t(),
            allowed_o_auth_scopes: [String.t()],
            analytics_configuration: %{
              application_id: String.t(),
              external_id: String.t(),
              role_arn: String.t(),
              user_data_shared: String.t()
            },
            callback_u_r_ls: [String.t()],
            client_id: String.t(),
            client_name: String.t(),
            client_secret: String.t(),
            creation_date: String.t(),
            default_redirect_u_r_i: String.t(),
            explicit_auth_flows: [String.t()],
            last_modified_date: String.t(),
            logout_u_r_ls: [String.t()],
            prevent_user_existence_errors: String.t(),
            read_attributes: [String.t()],
            refresh_token_validity: String.t(),
            supported_identity_providers: [String.t()],
            user_pool_id: String.t(),
            write_attributes: [String.t()]
          }
        }

  @spec create_user_pool_client(AwsSdk.Core.client(), create_user_pool_client_request) ::
          {:ok, create_user_pool_client_response} | {:error, AwsSdk.Core.aws_error()}

  def create_user_pool_client(client, create_user_pool_client_request) do
    make_request(client, "CreateUserPoolClient", create_user_pool_client_request)
  end

  @doc """
  Creates a new domain for a user pool.
  """

  @type create_user_pool_domain_request :: %{
          custom_domain_config: %{certificate_arn: String.t()},
          domain: String.t(),
          user_pool_id: String.t()
        }

  @type create_user_pool_domain_response :: %{cloud_front_domain: String.t()}

  @spec create_user_pool_domain(AwsSdk.Core.client(), create_user_pool_domain_request) ::
          {:ok, create_user_pool_domain_response} | {:error, AwsSdk.Core.aws_error()}

  def create_user_pool_domain(client, create_user_pool_domain_request) do
    make_request(client, "CreateUserPoolDomain", create_user_pool_domain_request)
  end

  @doc """
  Deletes a group. Currently only groups with no members can be deleted.
  Calling this action requires developer credentials.
  """

  @type delete_group_request :: %{group_name: String.t(), user_pool_id: String.t()}

  @type delete_group_response :: nil

  @spec delete_group(AwsSdk.Core.client(), delete_group_request) ::
          {:ok, delete_group_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_group(client, delete_group_request) do
    make_request(client, "DeleteGroup", delete_group_request)
  end

  @doc """
  Deletes an identity provider for a user pool.
  """

  @type delete_identity_provider_request :: %{provider_name: String.t(), user_pool_id: String.t()}

  @type delete_identity_provider_response :: nil

  @spec delete_identity_provider(AwsSdk.Core.client(), delete_identity_provider_request) ::
          {:ok, delete_identity_provider_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_identity_provider(client, delete_identity_provider_request) do
    make_request(client, "DeleteIdentityProvider", delete_identity_provider_request)
  end

  @doc """
  Deletes a resource server.
  """

  @type delete_resource_server_request :: %{identifier: String.t(), user_pool_id: String.t()}

  @type delete_resource_server_response :: nil

  @spec delete_resource_server(AwsSdk.Core.client(), delete_resource_server_request) ::
          {:ok, delete_resource_server_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_resource_server(client, delete_resource_server_request) do
    make_request(client, "DeleteResourceServer", delete_resource_server_request)
  end

  @doc """
  Allows a user to delete himself or herself.
  """

  @type delete_user_request :: %{access_token: String.t()}

  @type delete_user_response :: nil

  @spec delete_user(AwsSdk.Core.client(), delete_user_request) ::
          {:ok, delete_user_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_user(client, delete_user_request) do
    make_request(client, "DeleteUser", delete_user_request)
  end

  @doc """
  Deletes the attributes for a user.
  """

  @type delete_user_attributes_request :: %{
          access_token: String.t(),
          user_attribute_names: [String.t()]
        }

  @type delete_user_attributes_response :: nil

  @spec delete_user_attributes(AwsSdk.Core.client(), delete_user_attributes_request) ::
          {:ok, delete_user_attributes_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_user_attributes(client, delete_user_attributes_request) do
    make_request(client, "DeleteUserAttributes", delete_user_attributes_request)
  end

  @doc """
  Deletes the specified Amazon Cognito user pool.
  """

  @type delete_user_pool_request :: %{user_pool_id: String.t()}

  @type delete_user_pool_response :: nil

  @spec delete_user_pool(AwsSdk.Core.client(), delete_user_pool_request) ::
          {:ok, delete_user_pool_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_user_pool(client, delete_user_pool_request) do
    make_request(client, "DeleteUserPool", delete_user_pool_request)
  end

  @doc """
  Allows the developer to delete the user pool client.
  """

  @type delete_user_pool_client_request :: %{client_id: String.t(), user_pool_id: String.t()}

  @type delete_user_pool_client_response :: nil

  @spec delete_user_pool_client(AwsSdk.Core.client(), delete_user_pool_client_request) ::
          {:ok, delete_user_pool_client_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_user_pool_client(client, delete_user_pool_client_request) do
    make_request(client, "DeleteUserPoolClient", delete_user_pool_client_request)
  end

  @doc """
  Deletes a domain for a user pool.
  """

  @type delete_user_pool_domain_request :: %{domain: String.t(), user_pool_id: String.t()}

  @type delete_user_pool_domain_response :: nil

  @spec delete_user_pool_domain(AwsSdk.Core.client(), delete_user_pool_domain_request) ::
          {:ok, delete_user_pool_domain_response} | {:error, AwsSdk.Core.aws_error()}

  def delete_user_pool_domain(client, delete_user_pool_domain_request) do
    make_request(client, "DeleteUserPoolDomain", delete_user_pool_domain_request)
  end

  @doc """
  Gets information about a specific identity provider.
  """

  @type describe_identity_provider_request :: %{
          provider_name: String.t(),
          user_pool_id: String.t()
        }

  @type describe_identity_provider_response :: %{
          identity_provider: %{
            attribute_mapping: %{String.t() => String.t()},
            creation_date: String.t(),
            idp_identifiers: [String.t()],
            last_modified_date: String.t(),
            provider_details: %{String.t() => String.t()},
            provider_name: String.t(),
            provider_type: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec describe_identity_provider(AwsSdk.Core.client(), describe_identity_provider_request) ::
          {:ok, describe_identity_provider_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_identity_provider(client, describe_identity_provider_request) do
    make_request(client, "DescribeIdentityProvider", describe_identity_provider_request)
  end

  @doc """
  Describes a resource server.
  """

  @type describe_resource_server_request :: %{identifier: String.t(), user_pool_id: String.t()}

  @type describe_resource_server_response :: %{
          resource_server: %{
            identifier: String.t(),
            name: String.t(),
            scopes: [%{scope_description: String.t(), scope_name: String.t()}],
            user_pool_id: String.t()
          }
        }

  @spec describe_resource_server(AwsSdk.Core.client(), describe_resource_server_request) ::
          {:ok, describe_resource_server_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_resource_server(client, describe_resource_server_request) do
    make_request(client, "DescribeResourceServer", describe_resource_server_request)
  end

  @doc """
  Describes the risk configuration.
  """

  @type describe_risk_configuration_request :: %{client_id: String.t(), user_pool_id: String.t()}

  @type describe_risk_configuration_response :: %{
          risk_configuration: %{
            account_takeover_risk_configuration: %{
              actions: %{
                high_action: %{event_action: String.t(), notify: String.t()},
                low_action: %{event_action: String.t(), notify: String.t()},
                medium_action: %{event_action: String.t(), notify: String.t()}
              },
              notify_configuration: %{
                block_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
                from: String.t(),
                mfa_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
                no_action_email: %{
                  html_body: String.t(),
                  subject: String.t(),
                  text_body: String.t()
                },
                reply_to: String.t(),
                source_arn: String.t()
              }
            },
            client_id: String.t(),
            compromised_credentials_risk_configuration: %{
              actions: %{event_action: String.t()},
              event_filter: [String.t()]
            },
            last_modified_date: String.t(),
            risk_exception_configuration: %{
              blocked_i_p_range_list: [String.t()],
              skipped_i_p_range_list: [String.t()]
            },
            user_pool_id: String.t()
          }
        }

  @spec describe_risk_configuration(AwsSdk.Core.client(), describe_risk_configuration_request) ::
          {:ok, describe_risk_configuration_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_risk_configuration(client, describe_risk_configuration_request) do
    make_request(client, "DescribeRiskConfiguration", describe_risk_configuration_request)
  end

  @doc """
  Describes the user import job.
  """

  @type describe_user_import_job_request :: %{job_id: String.t(), user_pool_id: String.t()}

  @type describe_user_import_job_response :: %{
          user_import_job: %{
            cloud_watch_logs_role_arn: String.t(),
            completion_date: String.t(),
            completion_message: String.t(),
            creation_date: String.t(),
            failed_users: String.t(),
            imported_users: String.t(),
            job_id: String.t(),
            job_name: String.t(),
            pre_signed_url: String.t(),
            skipped_users: String.t(),
            start_date: String.t(),
            status: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec describe_user_import_job(AwsSdk.Core.client(), describe_user_import_job_request) ::
          {:ok, describe_user_import_job_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_user_import_job(client, describe_user_import_job_request) do
    make_request(client, "DescribeUserImportJob", describe_user_import_job_request)
  end

  @doc """
  Returns the configuration information and metadata of the specified user pool.
  """

  @type describe_user_pool_request :: %{user_pool_id: String.t()}

  @type describe_user_pool_response :: %{
          user_pool: %{
            account_recovery_setting: %{
              recovery_mechanisms: [%{name: String.t(), priority: String.t()}]
            },
            admin_create_user_config: %{
              allow_admin_create_user_only: String.t(),
              invite_message_template: %{
                email_message: String.t(),
                email_subject: String.t(),
                s_m_s_message: String.t()
              },
              unused_account_validity_days: String.t()
            },
            alias_attributes: [String.t()],
            arn: String.t(),
            auto_verified_attributes: [String.t()],
            creation_date: String.t(),
            custom_domain: String.t(),
            device_configuration: %{
              challenge_required_on_new_device: String.t(),
              device_only_remembered_on_user_prompt: String.t()
            },
            domain: String.t(),
            email_configuration: %{
              configuration_set: String.t(),
              email_sending_account: String.t(),
              from: String.t(),
              reply_to_email_address: String.t(),
              source_arn: String.t()
            },
            email_configuration_failure: String.t(),
            email_verification_message: String.t(),
            email_verification_subject: String.t(),
            estimated_number_of_users: String.t(),
            id: String.t(),
            lambda_config: %{
              create_auth_challenge: String.t(),
              custom_message: String.t(),
              define_auth_challenge: String.t(),
              post_authentication: String.t(),
              post_confirmation: String.t(),
              pre_authentication: String.t(),
              pre_sign_up: String.t(),
              pre_token_generation: String.t(),
              user_migration: String.t(),
              verify_auth_challenge_response: String.t()
            },
            last_modified_date: String.t(),
            mfa_configuration: String.t(),
            name: String.t(),
            policies: %{
              password_policy: %{
                minimum_length: String.t(),
                require_lowercase: String.t(),
                require_numbers: String.t(),
                require_symbols: String.t(),
                require_uppercase: String.t(),
                temporary_password_validity_days: String.t()
              }
            },
            schema_attributes: [
              %{
                attribute_data_type: String.t(),
                developer_only_attribute: String.t(),
                mutable: String.t(),
                name: String.t(),
                number_attribute_constraints: %{max_value: String.t(), min_value: String.t()},
                required: String.t(),
                string_attribute_constraints: %{max_length: String.t(), min_length: String.t()}
              }
            ],
            sms_authentication_message: String.t(),
            sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()},
            sms_configuration_failure: String.t(),
            sms_verification_message: String.t(),
            status: String.t(),
            user_pool_add_ons: %{advanced_security_mode: String.t()},
            user_pool_tags: %{String.t() => String.t()},
            username_attributes: [String.t()],
            verification_message_template: %{
              default_email_option: String.t(),
              email_message: String.t(),
              email_message_by_link: String.t(),
              email_subject: String.t(),
              email_subject_by_link: String.t(),
              sms_message: String.t()
            }
          }
        }

  @spec describe_user_pool(AwsSdk.Core.client(), describe_user_pool_request) ::
          {:ok, describe_user_pool_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_user_pool(client, describe_user_pool_request) do
    make_request(client, "DescribeUserPool", describe_user_pool_request)
  end

  @doc """
  Client method for returning the configuration information and metadata of the specified user pool app client.
  """

  @type describe_user_pool_client_request :: %{client_id: String.t(), user_pool_id: String.t()}

  @type describe_user_pool_client_response :: %{
          user_pool_client: %{
            allowed_o_auth_flows: [String.t()],
            allowed_o_auth_flows_user_pool_client: String.t(),
            allowed_o_auth_scopes: [String.t()],
            analytics_configuration: %{
              application_id: String.t(),
              external_id: String.t(),
              role_arn: String.t(),
              user_data_shared: String.t()
            },
            callback_u_r_ls: [String.t()],
            client_id: String.t(),
            client_name: String.t(),
            client_secret: String.t(),
            creation_date: String.t(),
            default_redirect_u_r_i: String.t(),
            explicit_auth_flows: [String.t()],
            last_modified_date: String.t(),
            logout_u_r_ls: [String.t()],
            prevent_user_existence_errors: String.t(),
            read_attributes: [String.t()],
            refresh_token_validity: String.t(),
            supported_identity_providers: [String.t()],
            user_pool_id: String.t(),
            write_attributes: [String.t()]
          }
        }

  @spec describe_user_pool_client(AwsSdk.Core.client(), describe_user_pool_client_request) ::
          {:ok, describe_user_pool_client_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_user_pool_client(client, describe_user_pool_client_request) do
    make_request(client, "DescribeUserPoolClient", describe_user_pool_client_request)
  end

  @doc """
  Gets information about a domain.
  """

  @type describe_user_pool_domain_request :: %{domain: String.t()}

  @type describe_user_pool_domain_response :: %{
          domain_description: %{
            a_w_s_account_id: String.t(),
            cloud_front_distribution: String.t(),
            custom_domain_config: %{certificate_arn: String.t()},
            domain: String.t(),
            s_3_bucket: String.t(),
            status: String.t(),
            user_pool_id: String.t(),
            version: String.t()
          }
        }

  @spec describe_user_pool_domain(AwsSdk.Core.client(), describe_user_pool_domain_request) ::
          {:ok, describe_user_pool_domain_response} | {:error, AwsSdk.Core.aws_error()}

  def describe_user_pool_domain(client, describe_user_pool_domain_request) do
    make_request(client, "DescribeUserPoolDomain", describe_user_pool_domain_request)
  end

  @doc """
  Forgets the specified device.
  """

  @type forget_device_request :: %{access_token: String.t(), device_key: String.t()}

  @type forget_device_response :: nil

  @spec forget_device(AwsSdk.Core.client(), forget_device_request) ::
          {:ok, forget_device_response} | {:error, AwsSdk.Core.aws_error()}

  def forget_device(client, forget_device_request) do
    make_request(client, "ForgetDevice", forget_device_request)
  end

  @doc """
  Calling this API causes a message to be sent to the end user with a confirmation code that is required to change the user's password. For the
  parameter, you can use the username or user alias. If a verified phone number exists for the user, the confirmation code is sent to the phone number. Otherwise, if a verified email exists, the confirmation code is sent to the email. If neither a verified phone number nor a verified email exists,
  is thrown. To use the confirmation code for resetting the password, call
  .
  """

  @type forgot_password_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          secret_hash: String.t(),
          user_context_data: %{encoded_data: String.t()},
          username: String.t()
        }

  @type forgot_password_response :: %{
          code_delivery_details: %{
            attribute_name: String.t(),
            delivery_medium: String.t(),
            destination: String.t()
          }
        }

  @spec forgot_password(AwsSdk.Core.client(), forgot_password_request) ::
          {:ok, forgot_password_response} | {:error, AwsSdk.Core.aws_error()}

  def forgot_password(client, forgot_password_request) do
    make_request(client, "ForgotPassword", forgot_password_request)
  end

  @doc """
  Gets the header information for the .csv file to be used as input for the user import job.
  """

  @type get_c_s_v_header_request :: %{user_pool_id: String.t()}

  @type get_c_s_v_header_response :: %{c_s_v_header: [String.t()], user_pool_id: String.t()}

  @spec get_c_s_v_header(AwsSdk.Core.client(), get_c_s_v_header_request) ::
          {:ok, get_c_s_v_header_response} | {:error, AwsSdk.Core.aws_error()}

  def get_c_s_v_header(client, get_c_s_v_header_request) do
    make_request(client, "GetCSVHeader", get_c_s_v_header_request)
  end

  @doc """
  Gets the device.
  """

  @type get_device_request :: %{access_token: String.t(), device_key: String.t()}

  @type get_device_response :: %{
          device: %{
            device_attributes: [%{name: String.t(), value: String.t()}],
            device_create_date: String.t(),
            device_key: String.t(),
            device_last_authenticated_date: String.t(),
            device_last_modified_date: String.t()
          }
        }

  @spec get_device(AwsSdk.Core.client(), get_device_request) ::
          {:ok, get_device_response} | {:error, AwsSdk.Core.aws_error()}

  def get_device(client, get_device_request) do
    make_request(client, "GetDevice", get_device_request)
  end

  @doc """
  Gets a group.
  Calling this action requires developer credentials.
  """

  @type get_group_request :: %{group_name: String.t(), user_pool_id: String.t()}

  @type get_group_response :: %{
          group: %{
            creation_date: String.t(),
            description: String.t(),
            group_name: String.t(),
            last_modified_date: String.t(),
            precedence: String.t(),
            role_arn: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec get_group(AwsSdk.Core.client(), get_group_request) ::
          {:ok, get_group_response} | {:error, AwsSdk.Core.aws_error()}

  def get_group(client, get_group_request) do
    make_request(client, "GetGroup", get_group_request)
  end

  @doc """
  Gets the specified identity provider.
  """

  @type get_identity_provider_by_identifier_request :: %{
          idp_identifier: String.t(),
          user_pool_id: String.t()
        }

  @type get_identity_provider_by_identifier_response :: %{
          identity_provider: %{
            attribute_mapping: %{String.t() => String.t()},
            creation_date: String.t(),
            idp_identifiers: [String.t()],
            last_modified_date: String.t(),
            provider_details: %{String.t() => String.t()},
            provider_name: String.t(),
            provider_type: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec get_identity_provider_by_identifier(
          AwsSdk.Core.client(),
          get_identity_provider_by_identifier_request
        ) ::
          {:ok, get_identity_provider_by_identifier_response} | {:error, AwsSdk.Core.aws_error()}

  def get_identity_provider_by_identifier(client, get_identity_provider_by_identifier_request) do
    make_request(
      client,
      "GetIdentityProviderByIdentifier",
      get_identity_provider_by_identifier_request
    )
  end

  @doc """
  This method takes a user pool ID, and returns the signing certificate.
  """

  @type get_signing_certificate_request :: %{user_pool_id: String.t()}

  @type get_signing_certificate_response :: %{certificate: String.t()}

  @spec get_signing_certificate(AwsSdk.Core.client(), get_signing_certificate_request) ::
          {:ok, get_signing_certificate_response} | {:error, AwsSdk.Core.aws_error()}

  def get_signing_certificate(client, get_signing_certificate_request) do
    make_request(client, "GetSigningCertificate", get_signing_certificate_request)
  end

  @doc """
  Gets the UI Customization information for a particular app client's app UI, if there is something set. If nothing is set for the particular client, but there is an existing pool level customization (app
  will be
  ), then that is returned. If nothing is present, then an empty shape is returned.
  """

  @type get_u_i_customization_request :: %{client_id: String.t(), user_pool_id: String.t()}

  @type get_u_i_customization_response :: %{
          u_i_customization: %{
            c_s_s: String.t(),
            c_s_s_version: String.t(),
            client_id: String.t(),
            creation_date: String.t(),
            image_url: String.t(),
            last_modified_date: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec get_u_i_customization(AwsSdk.Core.client(), get_u_i_customization_request) ::
          {:ok, get_u_i_customization_response} | {:error, AwsSdk.Core.aws_error()}

  def get_u_i_customization(client, get_u_i_customization_request) do
    make_request(client, "GetUICustomization", get_u_i_customization_request)
  end

  @doc """
  Gets the user attributes and metadata for a user.
  """

  @type get_user_request :: %{access_token: String.t()}

  @type get_user_response :: %{
          m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
          preferred_mfa_setting: String.t(),
          user_attributes: [%{name: String.t(), value: String.t()}],
          user_m_f_a_setting_list: [String.t()],
          username: String.t()
        }

  @spec get_user(AwsSdk.Core.client(), get_user_request) ::
          {:ok, get_user_response} | {:error, AwsSdk.Core.aws_error()}

  def get_user(client, get_user_request) do
    make_request(client, "GetUser", get_user_request)
  end

  @doc """
  Gets the user attribute verification code for the specified attribute name.
  """

  @type get_user_attribute_verification_code_request :: %{
          access_token: String.t(),
          attribute_name: String.t(),
          client_metadata: %{String.t() => String.t()}
        }

  @type get_user_attribute_verification_code_response :: %{
          code_delivery_details: %{
            attribute_name: String.t(),
            delivery_medium: String.t(),
            destination: String.t()
          }
        }

  @spec get_user_attribute_verification_code(
          AwsSdk.Core.client(),
          get_user_attribute_verification_code_request
        ) ::
          {:ok, get_user_attribute_verification_code_response} | {:error, AwsSdk.Core.aws_error()}

  def get_user_attribute_verification_code(client, get_user_attribute_verification_code_request) do
    make_request(
      client,
      "GetUserAttributeVerificationCode",
      get_user_attribute_verification_code_request
    )
  end

  @doc """
  Gets the user pool multi-factor authentication (MFA) configuration.
  """

  @type get_user_pool_mfa_config_request :: %{user_pool_id: String.t()}

  @type get_user_pool_mfa_config_response :: %{
          mfa_configuration: String.t(),
          sms_mfa_configuration: %{
            sms_authentication_message: String.t(),
            sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()}
          },
          software_token_mfa_configuration: %{enabled: String.t()}
        }

  @spec get_user_pool_mfa_config(AwsSdk.Core.client(), get_user_pool_mfa_config_request) ::
          {:ok, get_user_pool_mfa_config_response} | {:error, AwsSdk.Core.aws_error()}

  def get_user_pool_mfa_config(client, get_user_pool_mfa_config_request) do
    make_request(client, "GetUserPoolMfaConfig", get_user_pool_mfa_config_request)
  end

  @doc """
  Signs out users from all devices. It also invalidates all refresh tokens issued to a user. The user's current access and Id tokens remain valid until their expiry. Access and Id tokens expire one hour after they are issued.
  """

  @type global_sign_out_request :: %{access_token: String.t()}

  @type global_sign_out_response :: nil

  @spec global_sign_out(AwsSdk.Core.client(), global_sign_out_request) ::
          {:ok, global_sign_out_response} | {:error, AwsSdk.Core.aws_error()}

  def global_sign_out(client, global_sign_out_request) do
    make_request(client, "GlobalSignOut", global_sign_out_request)
  end

  @doc """
  Initiates the authentication flow.
  """

  @type initiate_auth_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          auth_flow: String.t(),
          auth_parameters: %{String.t() => String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          user_context_data: %{encoded_data: String.t()}
        }

  @type initiate_auth_response :: %{
          authentication_result: %{
            access_token: String.t(),
            expires_in: String.t(),
            id_token: String.t(),
            new_device_metadata: %{device_group_key: String.t(), device_key: String.t()},
            refresh_token: String.t(),
            token_type: String.t()
          },
          challenge_name: String.t(),
          challenge_parameters: %{String.t() => String.t()},
          session: String.t()
        }

  @spec initiate_auth(AwsSdk.Core.client(), initiate_auth_request) ::
          {:ok, initiate_auth_response} | {:error, AwsSdk.Core.aws_error()}

  def initiate_auth(client, initiate_auth_request) do
    make_request(client, "InitiateAuth", initiate_auth_request)
  end

  @doc """
  Lists the devices.
  """

  @type list_devices_request :: %{
          access_token: String.t(),
          limit: String.t(),
          pagination_token: String.t()
        }

  @type list_devices_response :: %{
          devices: [
            %{
              device_attributes: [%{name: String.t(), value: String.t()}],
              device_create_date: String.t(),
              device_key: String.t(),
              device_last_authenticated_date: String.t(),
              device_last_modified_date: String.t()
            }
          ],
          pagination_token: String.t()
        }

  @spec list_devices(AwsSdk.Core.client(), list_devices_request) ::
          {:ok, list_devices_response} | {:error, AwsSdk.Core.aws_error()}

  def list_devices(client, list_devices_request) do
    make_request(client, "ListDevices", list_devices_request)
  end

  @doc """
  Lists the groups associated with a user pool.
  Calling this action requires developer credentials.
  """

  @type list_groups_request :: %{
          limit: String.t(),
          next_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_groups_response :: %{
          groups: [
            %{
              creation_date: String.t(),
              description: String.t(),
              group_name: String.t(),
              last_modified_date: String.t(),
              precedence: String.t(),
              role_arn: String.t(),
              user_pool_id: String.t()
            }
          ],
          next_token: String.t()
        }

  @spec list_groups(AwsSdk.Core.client(), list_groups_request) ::
          {:ok, list_groups_response} | {:error, AwsSdk.Core.aws_error()}

  def list_groups(client, list_groups_request) do
    make_request(client, "ListGroups", list_groups_request)
  end

  @doc """
  Lists information about all identity providers for a user pool.
  """

  @type list_identity_providers_request :: %{
          max_results: String.t(),
          next_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_identity_providers_response :: %{
          next_token: String.t(),
          providers: [
            %{
              creation_date: String.t(),
              last_modified_date: String.t(),
              provider_name: String.t(),
              provider_type: String.t()
            }
          ]
        }

  @spec list_identity_providers(AwsSdk.Core.client(), list_identity_providers_request) ::
          {:ok, list_identity_providers_response} | {:error, AwsSdk.Core.aws_error()}

  def list_identity_providers(client, list_identity_providers_request) do
    make_request(client, "ListIdentityProviders", list_identity_providers_request)
  end

  @doc """
  Lists the resource servers for a user pool.
  """

  @type list_resource_servers_request :: %{
          max_results: String.t(),
          next_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_resource_servers_response :: %{
          next_token: String.t(),
          resource_servers: [
            %{
              identifier: String.t(),
              name: String.t(),
              scopes: [%{scope_description: String.t(), scope_name: String.t()}],
              user_pool_id: String.t()
            }
          ]
        }

  @spec list_resource_servers(AwsSdk.Core.client(), list_resource_servers_request) ::
          {:ok, list_resource_servers_response} | {:error, AwsSdk.Core.aws_error()}

  def list_resource_servers(client, list_resource_servers_request) do
    make_request(client, "ListResourceServers", list_resource_servers_request)
  end

  @doc """
  Lists the tags that are assigned to an Amazon Cognito user pool.
  A tag is a label that you can apply to user pools to categorize and manage them in different ways, such as by purpose, owner, environment, or other criteria.
  You can use this action up to 10 times per second, per account.
  """

  @type list_tags_for_resource_request :: %{resource_arn: String.t()}

  @type list_tags_for_resource_response :: %{tags: %{String.t() => String.t()}}

  @spec list_tags_for_resource(AwsSdk.Core.client(), list_tags_for_resource_request) ::
          {:ok, list_tags_for_resource_response} | {:error, AwsSdk.Core.aws_error()}

  def list_tags_for_resource(client, list_tags_for_resource_request) do
    make_request(client, "ListTagsForResource", list_tags_for_resource_request)
  end

  @doc """
  Lists the user import jobs.
  """

  @type list_user_import_jobs_request :: %{
          max_results: String.t(),
          pagination_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_user_import_jobs_response :: %{
          pagination_token: String.t(),
          user_import_jobs: [
            %{
              cloud_watch_logs_role_arn: String.t(),
              completion_date: String.t(),
              completion_message: String.t(),
              creation_date: String.t(),
              failed_users: String.t(),
              imported_users: String.t(),
              job_id: String.t(),
              job_name: String.t(),
              pre_signed_url: String.t(),
              skipped_users: String.t(),
              start_date: String.t(),
              status: String.t(),
              user_pool_id: String.t()
            }
          ]
        }

  @spec list_user_import_jobs(AwsSdk.Core.client(), list_user_import_jobs_request) ::
          {:ok, list_user_import_jobs_response} | {:error, AwsSdk.Core.aws_error()}

  def list_user_import_jobs(client, list_user_import_jobs_request) do
    make_request(client, "ListUserImportJobs", list_user_import_jobs_request)
  end

  @doc """
  Lists the clients that have been created for the specified user pool.
  """

  @type list_user_pool_clients_request :: %{
          max_results: String.t(),
          next_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_user_pool_clients_response :: %{
          next_token: String.t(),
          user_pool_clients: [
            %{client_id: String.t(), client_name: String.t(), user_pool_id: String.t()}
          ]
        }

  @spec list_user_pool_clients(AwsSdk.Core.client(), list_user_pool_clients_request) ::
          {:ok, list_user_pool_clients_response} | {:error, AwsSdk.Core.aws_error()}

  def list_user_pool_clients(client, list_user_pool_clients_request) do
    make_request(client, "ListUserPoolClients", list_user_pool_clients_request)
  end

  @doc """
  Lists the user pools associated with an AWS account.
  """

  @type list_user_pools_request :: %{max_results: String.t(), next_token: String.t()}

  @type list_user_pools_response :: %{
          next_token: String.t(),
          user_pools: [
            %{
              creation_date: String.t(),
              id: String.t(),
              lambda_config: %{
                create_auth_challenge: String.t(),
                custom_message: String.t(),
                define_auth_challenge: String.t(),
                post_authentication: String.t(),
                post_confirmation: String.t(),
                pre_authentication: String.t(),
                pre_sign_up: String.t(),
                pre_token_generation: String.t(),
                user_migration: String.t(),
                verify_auth_challenge_response: String.t()
              },
              last_modified_date: String.t(),
              name: String.t(),
              status: String.t()
            }
          ]
        }

  @spec list_user_pools(AwsSdk.Core.client(), list_user_pools_request) ::
          {:ok, list_user_pools_response} | {:error, AwsSdk.Core.aws_error()}

  def list_user_pools(client, list_user_pools_request) do
    make_request(client, "ListUserPools", list_user_pools_request)
  end

  @doc """
  Lists the users in the Amazon Cognito user pool.
  """

  @type list_users_request :: %{
          attributes_to_get: [String.t()],
          filter: String.t(),
          limit: String.t(),
          pagination_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_users_response :: %{
          pagination_token: String.t(),
          users: [
            %{
              attributes: [%{name: String.t(), value: String.t()}],
              enabled: String.t(),
              m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
              user_create_date: String.t(),
              user_last_modified_date: String.t(),
              user_status: String.t(),
              username: String.t()
            }
          ]
        }

  @spec list_users(AwsSdk.Core.client(), list_users_request) ::
          {:ok, list_users_response} | {:error, AwsSdk.Core.aws_error()}

  def list_users(client, list_users_request) do
    make_request(client, "ListUsers", list_users_request)
  end

  @doc """
  Lists the users in the specified group.
  Calling this action requires developer credentials.
  """

  @type list_users_in_group_request :: %{
          group_name: String.t(),
          limit: String.t(),
          next_token: String.t(),
          user_pool_id: String.t()
        }

  @type list_users_in_group_response :: %{
          next_token: String.t(),
          users: [
            %{
              attributes: [%{name: String.t(), value: String.t()}],
              enabled: String.t(),
              m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}],
              user_create_date: String.t(),
              user_last_modified_date: String.t(),
              user_status: String.t(),
              username: String.t()
            }
          ]
        }

  @spec list_users_in_group(AwsSdk.Core.client(), list_users_in_group_request) ::
          {:ok, list_users_in_group_response} | {:error, AwsSdk.Core.aws_error()}

  def list_users_in_group(client, list_users_in_group_request) do
    make_request(client, "ListUsersInGroup", list_users_in_group_request)
  end

  @doc """
  Resends the confirmation (for confirmation of registration) to a specific user in the user pool.
  """

  @type resend_confirmation_code_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          secret_hash: String.t(),
          user_context_data: %{encoded_data: String.t()},
          username: String.t()
        }

  @type resend_confirmation_code_response :: %{
          code_delivery_details: %{
            attribute_name: String.t(),
            delivery_medium: String.t(),
            destination: String.t()
          }
        }

  @spec resend_confirmation_code(AwsSdk.Core.client(), resend_confirmation_code_request) ::
          {:ok, resend_confirmation_code_response} | {:error, AwsSdk.Core.aws_error()}

  def resend_confirmation_code(client, resend_confirmation_code_request) do
    make_request(client, "ResendConfirmationCode", resend_confirmation_code_request)
  end

  @doc """
  Responds to the authentication challenge.
  """

  @type respond_to_auth_challenge_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          challenge_name: String.t(),
          challenge_responses: %{String.t() => String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          session: String.t(),
          user_context_data: %{encoded_data: String.t()}
        }

  @type respond_to_auth_challenge_response :: %{
          authentication_result: %{
            access_token: String.t(),
            expires_in: String.t(),
            id_token: String.t(),
            new_device_metadata: %{device_group_key: String.t(), device_key: String.t()},
            refresh_token: String.t(),
            token_type: String.t()
          },
          challenge_name: String.t(),
          challenge_parameters: %{String.t() => String.t()},
          session: String.t()
        }

  @spec respond_to_auth_challenge(AwsSdk.Core.client(), respond_to_auth_challenge_request) ::
          {:ok, respond_to_auth_challenge_response} | {:error, AwsSdk.Core.aws_error()}

  def respond_to_auth_challenge(client, respond_to_auth_challenge_request) do
    make_request(client, "RespondToAuthChallenge", respond_to_auth_challenge_request)
  end

  @doc """
  Configures actions on detected risks. To delete the risk configuration for
  or
  , pass null values for all four configuration types.
  To enable Amazon Cognito advanced security features, update the user pool to include the
  key
  .
  See
  .
  """

  @type set_risk_configuration_request :: %{
          account_takeover_risk_configuration: %{
            actions: %{
              high_action: %{event_action: String.t(), notify: String.t()},
              low_action: %{event_action: String.t(), notify: String.t()},
              medium_action: %{event_action: String.t(), notify: String.t()}
            },
            notify_configuration: %{
              block_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
              from: String.t(),
              mfa_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
              no_action_email: %{
                html_body: String.t(),
                subject: String.t(),
                text_body: String.t()
              },
              reply_to: String.t(),
              source_arn: String.t()
            }
          },
          client_id: String.t(),
          compromised_credentials_risk_configuration: %{
            actions: %{event_action: String.t()},
            event_filter: [String.t()]
          },
          risk_exception_configuration: %{
            blocked_i_p_range_list: [String.t()],
            skipped_i_p_range_list: [String.t()]
          },
          user_pool_id: String.t()
        }

  @type set_risk_configuration_response :: %{
          risk_configuration: %{
            account_takeover_risk_configuration: %{
              actions: %{
                high_action: %{event_action: String.t(), notify: String.t()},
                low_action: %{event_action: String.t(), notify: String.t()},
                medium_action: %{event_action: String.t(), notify: String.t()}
              },
              notify_configuration: %{
                block_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
                from: String.t(),
                mfa_email: %{html_body: String.t(), subject: String.t(), text_body: String.t()},
                no_action_email: %{
                  html_body: String.t(),
                  subject: String.t(),
                  text_body: String.t()
                },
                reply_to: String.t(),
                source_arn: String.t()
              }
            },
            client_id: String.t(),
            compromised_credentials_risk_configuration: %{
              actions: %{event_action: String.t()},
              event_filter: [String.t()]
            },
            last_modified_date: String.t(),
            risk_exception_configuration: %{
              blocked_i_p_range_list: [String.t()],
              skipped_i_p_range_list: [String.t()]
            },
            user_pool_id: String.t()
          }
        }

  @spec set_risk_configuration(AwsSdk.Core.client(), set_risk_configuration_request) ::
          {:ok, set_risk_configuration_response} | {:error, AwsSdk.Core.aws_error()}

  def set_risk_configuration(client, set_risk_configuration_request) do
    make_request(client, "SetRiskConfiguration", set_risk_configuration_request)
  end

  @doc """
  Sets the UI customization information for a user pool's built-in app UI.
  You can specify app UI customization settings for a single client (with a specific
  ) or for all clients (by setting the
  to
  ). If you specify
  , the default configuration will be used for every client that has no UI customization set previously. If you specify UI customization settings for a particular client, it will no longer fall back to the
  configuration.
  """

  @type set_u_i_customization_request :: %{
          c_s_s: String.t(),
          client_id: String.t(),
          image_file: String.t(),
          user_pool_id: String.t()
        }

  @type set_u_i_customization_response :: %{
          u_i_customization: %{
            c_s_s: String.t(),
            c_s_s_version: String.t(),
            client_id: String.t(),
            creation_date: String.t(),
            image_url: String.t(),
            last_modified_date: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec set_u_i_customization(AwsSdk.Core.client(), set_u_i_customization_request) ::
          {:ok, set_u_i_customization_response} | {:error, AwsSdk.Core.aws_error()}

  def set_u_i_customization(client, set_u_i_customization_request) do
    make_request(client, "SetUICustomization", set_u_i_customization_request)
  end

  @doc """
  Set the user's multi-factor authentication (MFA) method preference, including which MFA factors are enabled and if any are preferred. Only one factor can be set as preferred. The preferred MFA factor will be used to authenticate a user if multiple factors are enabled. If multiple options are enabled and no preference is set, a challenge to choose an MFA option will be returned during sign in.
  """

  @type set_user_m_f_a_preference_request :: %{
          access_token: String.t(),
          s_m_s_mfa_settings: %{enabled: String.t(), preferred_mfa: String.t()},
          software_token_mfa_settings: %{enabled: String.t(), preferred_mfa: String.t()}
        }

  @type set_user_m_f_a_preference_response :: nil

  @spec set_user_m_f_a_preference(AwsSdk.Core.client(), set_user_m_f_a_preference_request) ::
          {:ok, set_user_m_f_a_preference_response} | {:error, AwsSdk.Core.aws_error()}

  def set_user_m_f_a_preference(client, set_user_m_f_a_preference_request) do
    make_request(client, "SetUserMFAPreference", set_user_m_f_a_preference_request)
  end

  @doc """
  Set the user pool multi-factor authentication (MFA) configuration.
  """

  @type set_user_pool_mfa_config_request :: %{
          mfa_configuration: String.t(),
          sms_mfa_configuration: %{
            sms_authentication_message: String.t(),
            sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()}
          },
          software_token_mfa_configuration: %{enabled: String.t()},
          user_pool_id: String.t()
        }

  @type set_user_pool_mfa_config_response :: %{
          mfa_configuration: String.t(),
          sms_mfa_configuration: %{
            sms_authentication_message: String.t(),
            sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()}
          },
          software_token_mfa_configuration: %{enabled: String.t()}
        }

  @spec set_user_pool_mfa_config(AwsSdk.Core.client(), set_user_pool_mfa_config_request) ::
          {:ok, set_user_pool_mfa_config_response} | {:error, AwsSdk.Core.aws_error()}

  def set_user_pool_mfa_config(client, set_user_pool_mfa_config_request) do
    make_request(client, "SetUserPoolMfaConfig", set_user_pool_mfa_config_request)
  end

  @doc """
  You can use it to configure only SMS MFA. You can't use it to configure TOTP software token MFA. To configure either type of MFA, use the
  action instead.
  """

  @type set_user_settings_request :: %{
          access_token: String.t(),
          m_f_a_options: [%{attribute_name: String.t(), delivery_medium: String.t()}]
        }

  @type set_user_settings_response :: nil

  @spec set_user_settings(AwsSdk.Core.client(), set_user_settings_request) ::
          {:ok, set_user_settings_response} | {:error, AwsSdk.Core.aws_error()}

  def set_user_settings(client, set_user_settings_request) do
    make_request(client, "SetUserSettings", set_user_settings_request)
  end

  @doc """
  Registers the user in the specified user pool and creates a user name, password, and user attributes.
  """

  @type sign_up_request :: %{
          analytics_metadata: %{analytics_endpoint_id: String.t()},
          client_id: String.t(),
          client_metadata: %{String.t() => String.t()},
          password: String.t(),
          secret_hash: String.t(),
          user_attributes: [%{name: String.t(), value: String.t()}],
          user_context_data: %{encoded_data: String.t()},
          username: String.t(),
          validation_data: [%{name: String.t(), value: String.t()}]
        }

  @type sign_up_response :: %{
          code_delivery_details: %{
            attribute_name: String.t(),
            delivery_medium: String.t(),
            destination: String.t()
          },
          user_confirmed: String.t(),
          user_sub: String.t()
        }

  @spec sign_up(AwsSdk.Core.client(), sign_up_request) ::
          {:ok, sign_up_response} | {:error, AwsSdk.Core.aws_error()}

  def sign_up(client, sign_up_request) do
    make_request(client, "SignUp", sign_up_request)
  end

  @doc """
  Starts the user import.
  """

  @type start_user_import_job_request :: %{job_id: String.t(), user_pool_id: String.t()}

  @type start_user_import_job_response :: %{
          user_import_job: %{
            cloud_watch_logs_role_arn: String.t(),
            completion_date: String.t(),
            completion_message: String.t(),
            creation_date: String.t(),
            failed_users: String.t(),
            imported_users: String.t(),
            job_id: String.t(),
            job_name: String.t(),
            pre_signed_url: String.t(),
            skipped_users: String.t(),
            start_date: String.t(),
            status: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec start_user_import_job(AwsSdk.Core.client(), start_user_import_job_request) ::
          {:ok, start_user_import_job_response} | {:error, AwsSdk.Core.aws_error()}

  def start_user_import_job(client, start_user_import_job_request) do
    make_request(client, "StartUserImportJob", start_user_import_job_request)
  end

  @doc """
  Stops the user import job.
  """

  @type stop_user_import_job_request :: %{job_id: String.t(), user_pool_id: String.t()}

  @type stop_user_import_job_response :: %{
          user_import_job: %{
            cloud_watch_logs_role_arn: String.t(),
            completion_date: String.t(),
            completion_message: String.t(),
            creation_date: String.t(),
            failed_users: String.t(),
            imported_users: String.t(),
            job_id: String.t(),
            job_name: String.t(),
            pre_signed_url: String.t(),
            skipped_users: String.t(),
            start_date: String.t(),
            status: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec stop_user_import_job(AwsSdk.Core.client(), stop_user_import_job_request) ::
          {:ok, stop_user_import_job_response} | {:error, AwsSdk.Core.aws_error()}

  def stop_user_import_job(client, stop_user_import_job_request) do
    make_request(client, "StopUserImportJob", stop_user_import_job_request)
  end

  @doc """
  Assigns a set of tags to an Amazon Cognito user pool. A tag is a label that you can use to categorize and manage user pools in different ways, such as by purpose, owner, environment, or other criteria.
  Each tag consists of a key and value, both of which you define. A key is a general category for more specific values. For example, if you have two versions of a user pool, one for testing and another for production, you might assign an
  tag key to both user pools. The value of this key might be
  for one user pool and
  for the other.
  Tags are useful for cost tracking and access control. You can activate your tags so that they appear on the Billing and Cost Management console, where you can track the costs associated with your user pools. In an IAM policy, you can constrain permissions for user pools based on specific tags or tag values.
  You can use this action up to 5 times per second, per account. A user pool can have as many as 50 tags.
  """

  @type tag_resource_request :: %{resource_arn: String.t(), tags: %{String.t() => String.t()}}

  @type tag_resource_response :: nil

  @spec tag_resource(AwsSdk.Core.client(), tag_resource_request) ::
          {:ok, tag_resource_response} | {:error, AwsSdk.Core.aws_error()}

  def tag_resource(client, tag_resource_request) do
    make_request(client, "TagResource", tag_resource_request)
  end

  @doc """
  Removes the specified tags from an Amazon Cognito user pool. You can use this action up to 5 times per second, per account
  """

  @type untag_resource_request :: %{resource_arn: String.t(), tag_keys: [String.t()]}

  @type untag_resource_response :: nil

  @spec untag_resource(AwsSdk.Core.client(), untag_resource_request) ::
          {:ok, untag_resource_response} | {:error, AwsSdk.Core.aws_error()}

  def untag_resource(client, untag_resource_request) do
    make_request(client, "UntagResource", untag_resource_request)
  end

  @doc """
  Provides the feedback for an authentication event whether it was from a valid user or not. This feedback is used for improving the risk evaluation decision for the user pool as part of Amazon Cognito advanced security.
  """

  @type update_auth_event_feedback_request :: %{
          event_id: String.t(),
          feedback_token: String.t(),
          feedback_value: String.t(),
          user_pool_id: String.t(),
          username: String.t()
        }

  @type update_auth_event_feedback_response :: nil

  @spec update_auth_event_feedback(AwsSdk.Core.client(), update_auth_event_feedback_request) ::
          {:ok, update_auth_event_feedback_response} | {:error, AwsSdk.Core.aws_error()}

  def update_auth_event_feedback(client, update_auth_event_feedback_request) do
    make_request(client, "UpdateAuthEventFeedback", update_auth_event_feedback_request)
  end

  @doc """
  Updates the device status.
  """

  @type update_device_status_request :: %{
          access_token: String.t(),
          device_key: String.t(),
          device_remembered_status: String.t()
        }

  @type update_device_status_response :: nil

  @spec update_device_status(AwsSdk.Core.client(), update_device_status_request) ::
          {:ok, update_device_status_response} | {:error, AwsSdk.Core.aws_error()}

  def update_device_status(client, update_device_status_request) do
    make_request(client, "UpdateDeviceStatus", update_device_status_request)
  end

  @doc """
  Updates the specified group with the specified attributes.
  Calling this action requires developer credentials.
  """

  @type update_group_request :: %{
          description: String.t(),
          group_name: String.t(),
          precedence: String.t(),
          role_arn: String.t(),
          user_pool_id: String.t()
        }

  @type update_group_response :: %{
          group: %{
            creation_date: String.t(),
            description: String.t(),
            group_name: String.t(),
            last_modified_date: String.t(),
            precedence: String.t(),
            role_arn: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec update_group(AwsSdk.Core.client(), update_group_request) ::
          {:ok, update_group_response} | {:error, AwsSdk.Core.aws_error()}

  def update_group(client, update_group_request) do
    make_request(client, "UpdateGroup", update_group_request)
  end

  @doc """
  Updates identity provider information for a user pool.
  """

  @type update_identity_provider_request :: %{
          attribute_mapping: %{String.t() => String.t()},
          idp_identifiers: [String.t()],
          provider_details: %{String.t() => String.t()},
          provider_name: String.t(),
          user_pool_id: String.t()
        }

  @type update_identity_provider_response :: %{
          identity_provider: %{
            attribute_mapping: %{String.t() => String.t()},
            creation_date: String.t(),
            idp_identifiers: [String.t()],
            last_modified_date: String.t(),
            provider_details: %{String.t() => String.t()},
            provider_name: String.t(),
            provider_type: String.t(),
            user_pool_id: String.t()
          }
        }

  @spec update_identity_provider(AwsSdk.Core.client(), update_identity_provider_request) ::
          {:ok, update_identity_provider_response} | {:error, AwsSdk.Core.aws_error()}

  def update_identity_provider(client, update_identity_provider_request) do
    make_request(client, "UpdateIdentityProvider", update_identity_provider_request)
  end

  @doc """
  Updates the name and scopes of resource server. All other fields are read-only.
  """

  @type update_resource_server_request :: %{
          identifier: String.t(),
          name: String.t(),
          scopes: [%{scope_description: String.t(), scope_name: String.t()}],
          user_pool_id: String.t()
        }

  @type update_resource_server_response :: %{
          resource_server: %{
            identifier: String.t(),
            name: String.t(),
            scopes: [%{scope_description: String.t(), scope_name: String.t()}],
            user_pool_id: String.t()
          }
        }

  @spec update_resource_server(AwsSdk.Core.client(), update_resource_server_request) ::
          {:ok, update_resource_server_response} | {:error, AwsSdk.Core.aws_error()}

  def update_resource_server(client, update_resource_server_request) do
    make_request(client, "UpdateResourceServer", update_resource_server_request)
  end

  @doc """
  Allows a user to update a specific attribute (one at a time).
  """

  @type update_user_attributes_request :: %{
          access_token: String.t(),
          client_metadata: %{String.t() => String.t()},
          user_attributes: [%{name: String.t(), value: String.t()}]
        }

  @type update_user_attributes_response :: %{
          code_delivery_details_list: [
            %{attribute_name: String.t(), delivery_medium: String.t(), destination: String.t()}
          ]
        }

  @spec update_user_attributes(AwsSdk.Core.client(), update_user_attributes_request) ::
          {:ok, update_user_attributes_response} | {:error, AwsSdk.Core.aws_error()}

  def update_user_attributes(client, update_user_attributes_request) do
    make_request(client, "UpdateUserAttributes", update_user_attributes_request)
  end

  @doc """
  Updates the specified user pool with the specified attributes. You can get a list of the current user pool settings with
  .
  """

  @type update_user_pool_request :: %{
          account_recovery_setting: %{
            recovery_mechanisms: [%{name: String.t(), priority: String.t()}]
          },
          admin_create_user_config: %{
            allow_admin_create_user_only: String.t(),
            invite_message_template: %{
              email_message: String.t(),
              email_subject: String.t(),
              s_m_s_message: String.t()
            },
            unused_account_validity_days: String.t()
          },
          auto_verified_attributes: [String.t()],
          device_configuration: %{
            challenge_required_on_new_device: String.t(),
            device_only_remembered_on_user_prompt: String.t()
          },
          email_configuration: %{
            configuration_set: String.t(),
            email_sending_account: String.t(),
            from: String.t(),
            reply_to_email_address: String.t(),
            source_arn: String.t()
          },
          email_verification_message: String.t(),
          email_verification_subject: String.t(),
          lambda_config: %{
            create_auth_challenge: String.t(),
            custom_message: String.t(),
            define_auth_challenge: String.t(),
            post_authentication: String.t(),
            post_confirmation: String.t(),
            pre_authentication: String.t(),
            pre_sign_up: String.t(),
            pre_token_generation: String.t(),
            user_migration: String.t(),
            verify_auth_challenge_response: String.t()
          },
          mfa_configuration: String.t(),
          policies: %{
            password_policy: %{
              minimum_length: String.t(),
              require_lowercase: String.t(),
              require_numbers: String.t(),
              require_symbols: String.t(),
              require_uppercase: String.t(),
              temporary_password_validity_days: String.t()
            }
          },
          sms_authentication_message: String.t(),
          sms_configuration: %{external_id: String.t(), sns_caller_arn: String.t()},
          sms_verification_message: String.t(),
          user_pool_add_ons: %{advanced_security_mode: String.t()},
          user_pool_id: String.t(),
          user_pool_tags: %{String.t() => String.t()},
          verification_message_template: %{
            default_email_option: String.t(),
            email_message: String.t(),
            email_message_by_link: String.t(),
            email_subject: String.t(),
            email_subject_by_link: String.t(),
            sms_message: String.t()
          }
        }

  @type update_user_pool_response :: nil

  @spec update_user_pool(AwsSdk.Core.client(), update_user_pool_request) ::
          {:ok, update_user_pool_response} | {:error, AwsSdk.Core.aws_error()}

  def update_user_pool(client, update_user_pool_request) do
    make_request(client, "UpdateUserPool", update_user_pool_request)
  end

  @doc """
  Updates the specified user pool app client with the specified attributes. You can get a list of the current user pool app client settings with
  .
  """

  @type update_user_pool_client_request :: %{
          allowed_o_auth_flows: [String.t()],
          allowed_o_auth_flows_user_pool_client: String.t(),
          allowed_o_auth_scopes: [String.t()],
          analytics_configuration: %{
            application_id: String.t(),
            external_id: String.t(),
            role_arn: String.t(),
            user_data_shared: String.t()
          },
          callback_u_r_ls: [String.t()],
          client_id: String.t(),
          client_name: String.t(),
          default_redirect_u_r_i: String.t(),
          explicit_auth_flows: [String.t()],
          logout_u_r_ls: [String.t()],
          prevent_user_existence_errors: String.t(),
          read_attributes: [String.t()],
          refresh_token_validity: String.t(),
          supported_identity_providers: [String.t()],
          user_pool_id: String.t(),
          write_attributes: [String.t()]
        }

  @type update_user_pool_client_response :: %{
          user_pool_client: %{
            allowed_o_auth_flows: [String.t()],
            allowed_o_auth_flows_user_pool_client: String.t(),
            allowed_o_auth_scopes: [String.t()],
            analytics_configuration: %{
              application_id: String.t(),
              external_id: String.t(),
              role_arn: String.t(),
              user_data_shared: String.t()
            },
            callback_u_r_ls: [String.t()],
            client_id: String.t(),
            client_name: String.t(),
            client_secret: String.t(),
            creation_date: String.t(),
            default_redirect_u_r_i: String.t(),
            explicit_auth_flows: [String.t()],
            last_modified_date: String.t(),
            logout_u_r_ls: [String.t()],
            prevent_user_existence_errors: String.t(),
            read_attributes: [String.t()],
            refresh_token_validity: String.t(),
            supported_identity_providers: [String.t()],
            user_pool_id: String.t(),
            write_attributes: [String.t()]
          }
        }

  @spec update_user_pool_client(AwsSdk.Core.client(), update_user_pool_client_request) ::
          {:ok, update_user_pool_client_response} | {:error, AwsSdk.Core.aws_error()}

  def update_user_pool_client(client, update_user_pool_client_request) do
    make_request(client, "UpdateUserPoolClient", update_user_pool_client_request)
  end

  @doc """
  Updates the Secure Sockets Layer (SSL) certificate for the custom domain for your user pool.
  You can use this operation to provide the Amazon Resource Name (ARN) of a new certificate to Amazon Cognito. You cannot use it to change the domain for a user pool.
  A custom domain is used to host the Amazon Cognito hosted UI, which provides sign-up and sign-in pages for your application. When you set up a custom domain, you provide a certificate that you manage with AWS Certificate Manager (ACM). When necessary, you can use this operation to change the certificate that you applied to your custom domain.
  Usually, this is unnecessary following routine certificate renewal with ACM. When you renew your existing certificate in ACM, the ARN for your certificate remains the same, and your custom domain uses the new certificate automatically.
  However, if you replace your existing certificate with a new one, ACM gives the new certificate a new ARN. To apply the new certificate to your custom domain, you must provide this ARN to Amazon Cognito.
  When you add your new certificate in ACM, you must choose US East (N. Virginia) as the AWS Region.
  After you submit your request, Amazon Cognito requires up to 1 hour to distribute your new certificate to your custom domain.
  For more information about adding a custom domain to your user pool, see
  .
  """

  @type update_user_pool_domain_request :: %{
          custom_domain_config: %{certificate_arn: String.t()},
          domain: String.t(),
          user_pool_id: String.t()
        }

  @type update_user_pool_domain_response :: %{cloud_front_domain: String.t()}

  @spec update_user_pool_domain(AwsSdk.Core.client(), update_user_pool_domain_request) ::
          {:ok, update_user_pool_domain_response} | {:error, AwsSdk.Core.aws_error()}

  def update_user_pool_domain(client, update_user_pool_domain_request) do
    make_request(client, "UpdateUserPoolDomain", update_user_pool_domain_request)
  end

  @doc """
  Use this API to register a user's entered TOTP code and mark the user's software token MFA status as "verified" if successful. The request takes an access token or a session string, but not both.
  """

  @type verify_software_token_request :: %{
          access_token: String.t(),
          friendly_device_name: String.t(),
          session: String.t(),
          user_code: String.t()
        }

  @type verify_software_token_response :: %{session: String.t(), status: String.t()}

  @spec verify_software_token(AwsSdk.Core.client(), verify_software_token_request) ::
          {:ok, verify_software_token_response} | {:error, AwsSdk.Core.aws_error()}

  def verify_software_token(client, verify_software_token_request) do
    make_request(client, "VerifySoftwareToken", verify_software_token_request)
  end

  @doc """
  Verifies the specified user attributes in the user pool.
  """

  @type verify_user_attribute_request :: %{
          access_token: String.t(),
          attribute_name: String.t(),
          code: String.t()
        }

  @type verify_user_attribute_response :: nil

  @spec verify_user_attribute(AwsSdk.Core.client(), verify_user_attribute_request) ::
          {:ok, verify_user_attribute_response} | {:error, AwsSdk.Core.aws_error()}

  def verify_user_attribute(client, verify_user_attribute_request) do
    make_request(client, "VerifyUserAttribute", verify_user_attribute_request)
  end
end
